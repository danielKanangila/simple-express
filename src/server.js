import express from 'express';
import routes from './routes';
const PORT=4000

const app = express();

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

routes(app);

app.listen(PORT, () => {
    console.log('Application is running on port ', PORT)
});
