import authRouter from './auth.router';

export default (app) => {
    app.use('/api/v1/auth', authRouter)

    app.get('/health', (_, res) => {
        res.status(200);
        res.send("healthy");
    });
}