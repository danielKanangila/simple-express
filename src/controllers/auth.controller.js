import { Op } from 'sequelize';
import model from '../models';
import { hash } from '../lib/crypto';
import { sendErrorResponse, sendSuccessResponse } from '../lib/sendResponse';

const { User } = model;

export default {
  async signUp(req, res) {
    const {email, password, name, phone} = req.body;
    console.log(req.body)
    try {
      let user = await User.findOne({where: {[Op.or]: [ {phone}, {email} ]}});
      if(user) {
        return res.status(422)
        .send({message: 'User with that email or phone already exists'});
      }

      user = await User.create({
        name,
        email,
        password: hash(password),
        phone,
      });
      return sendSuccessResponse(res, 201, user)
    } catch(e) {
      console.log(e);
      return sendErrorResponse(res, 500, e.message || 'Could not perform operation at this time, kindly try again later.');
    }
  }
}