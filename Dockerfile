
FROM alpine:latest
RUN apk add --no-cache nodejs npm openssh-server

WORKDIR /app
COPY package.json /app
RUN npm isntall

COPY . /app

EXPOSE 4000
CMD ["npm", "start"]