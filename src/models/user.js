import { Model } from "sequelize";
import { hash } from "../lib/crypto";
import Random from "../lib/random";

const PROTECTED_ATTRIBUTES = ['password'];

export default (sequelize, DataTypes) => {
  class User extends Model {
    toJSON() {
      const attributes = { ...this.get() }
      for (const a of PROTECTED_ATTRIBUTES) {
        delete attributes[a]
      }
      return attributes;
    }
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) { //eslint-disable-line
      // define association here
    }
  }
  User.init({
    name: DataTypes.STRING,
    email: DataTypes.STRING,
    phone: DataTypes.STRING,
    password: DataTypes.STRING,
    status: DataTypes.STRING,
    last_login_at: DataTypes.DATE,
    last_ip_address: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'User',
  });

  User.prototype.newToken = async function newToken(deviceName = 'Web FE') {
    const plainTextToken = Random(40);

    const token = await this.createToken({
      name: deviceName,
      token: hash(plainTextToken)
    })

    return {
      accessToken: token,
      plainTextToken: `${token.id}|${plainTextToken}`
    }
  }

  return User;
};